//
//  SqliteService.swift
//  ClassApp
//
//  Created by admin on 26/08/2021.
//

import UIKit
import SQLite

class SqliteService: NSObject {
    static let shared: SqliteService = SqliteService()
    var DatabaseRoot:Connection?
    var listDataQuiz:[QuizModel] = [QuizModel]()
    var listDataGuess:[GuessModel] = [GuessModel]()
    var listDataWall:[WallModel] = [WallModel]()
    func loadinit() {
        let dbURL = Bundle.main.url(forResource: "mkas_final", withExtension: "db")!
        
        var newURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        newURL.appendPathComponent("mkas_final.db")
        do {
            if FileManager.default.fileExists(atPath: newURL.path) {
                print("sss")
            }
            try FileManager.default.copyItem(atPath: dbURL.path, toPath: newURL.path)
            print(newURL.path)
        } catch {
            print(error.localizedDescription)
        }
        
        do {
            DatabaseRoot = try Connection(newURL.path)
        } catch {
            DatabaseRoot = nil
            let nserr = error as NSError
            print("Cannot connect to Database. Error is: \(nserr), \(nserr.userInfo)")
        }
        
    }
    
    func getDataQuiz(closure: @escaping (_ response: [QuizModel]?, _ error: Error?) -> Void) {
        let users1 = Table("clash")
        let id = Expression<Int64>("id")
        let ans1 = Expression<String?>("ans1")
        let ans2 = Expression<String?>("ans2")
        let ans3 = Expression<String?>("ans3")
        let ques = Expression<String>("ques")
        let answer = Expression<String>("answer")
        listDataQuiz.removeAll()
        if let DatabaseRoot = DatabaseRoot{
            do{
                for user in try DatabaseRoot.prepare(users1) {
                    listDataQuiz.append(QuizModel(id: Int(user[id]),
                                                  ans1: user[ans1] ?? "",
                                                  ans2: user[ans2] ?? "",
                                                  ans3: user[ans3] ?? "",
                                                  ques: user[ques],
                                                  answer: user[answer]))
                }
            }
            catch {
            }
        }
        closure(listDataQuiz, nil)
        
    }
    func getDataWall(closure: @escaping (_ response: [WallModel]?, _ error: Error?) -> Void) {
        let users3 = Table("wall")
        let id = Expression<Int64>("id")
        let img = Expression<Blob?>("img")
        listDataWall.removeAll()
        if let DatabaseRoot = DatabaseRoot{
            do{
                for user in try DatabaseRoot.prepare(users3) {
                    listDataWall.append(WallModel(id: Int(user[id])
                                                    ,img: user[img] ?? Blob(bytes: [1])))
                }
            } catch  {
            }
        }
        closure(listDataWall, nil)
        
    }
    func getDataGuess(closure: @escaping (_ response: [GuessModel]?, _ error: Error?) -> Void) {
        let users2 = Table("quick")
        let id1 = Expression<Int64>("id")
        let ans1 = Expression<String>("ans")
        let img1 = Expression<Blob?>("img")
        listDataGuess.removeAll()
        if let DatabaseRoot = DatabaseRoot{
            do{
                for user in try DatabaseRoot.prepare(users2) {
                    listDataGuess.append(GuessModel(id: Int(user[id1])
                                                    , ans: user[ans1]
                                                    ,img: user[img1] ?? Blob(bytes: [1])))
                }
            } catch  {
            }
        }
        closure(listDataGuess, nil)
        
    }
    func getAmountLetterOfRightAnswer(number: Int)->Int{
        return getRightAnswerLetters(number: number).count
    }
    
    func setNumberOfSection(number: Int)->Int{
        let rightAnswer = getRightAnswer(number: number)
        return rightAnswer.count
    }
    
    func getRightAnswer(number: Int)->[String]{
        var word: [String] = []
        for item in listDataGuess{
            if item.id == number{
                word = item.ans.components(separatedBy: .whitespaces)
                break
            }
        }
        return word
    }
    
    func getOriginalRightAnswer(number: Int)->String{
        var word = ""
        for item in listDataGuess{
            if item.id == number{
                word = item.ans
                break
            }
        }
        return word
    }
    
    func getRightAnswerLettersIncludeWhiteSpace(number: Int)->[String]{
        var letter:[String] = []
        for item in listDataGuess{
            if item.id == number{
                let word = item.ans
                for item in 0...word.count-1{
                    let index = word.index(word.startIndex, offsetBy: item)
                    letter.append(String(word[index]).lowercased())
                }
                
                return letter
            }
        }
        return []
    }
    
    func getRightAnswerLetters(number: Int)->[String]{
        var rightAnswerLetters: [String] = []
        let rightAnswerLettersIncludeWhiteSpace = getRightAnswerLettersIncludeWhiteSpace(number: number)
        for item in rightAnswerLettersIncludeWhiteSpace{
            if item == " "{
                continue
            }
            rightAnswerLetters.append(item)
        }
        return rightAnswerLetters
    }
    
    func setNumberOfSection0(number: Int)->Int{
        let firstWord = getRightAnswer(number: number)[0]
        return firstWord.count
    }
    
    func setNumberOfSection1(number: Int)->Int{
        let secondWord = getRightAnswer(number: number)[1]
        return secondWord.count
    }
    
    
    func randomizeAvailableLetters(tileArraySize: Int) -> Array<String> {
        let alphabet: [String] = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
        var availableTiles = [String]()
        for _ in 0..<tileArraySize {
            let rand = Int(arc4random_uniform(26))
            availableTiles.append(alphabet[rand])
        }
        return(availableTiles)
    }
    
    func getAmountOfRandomLetters(number: Int)->Int{
        let amountOfRightAnswer = getAmountLetterOfRightAnswer(number: number)
        return 30 - amountOfRightAnswer
    }
    
    func shuffleLetters(number: Int)->[LetterModel]{
        var randomAndRightAnswerLetters: [LetterModel] = [LetterModel]()
        let amountOfRandomLetters = getAmountOfRandomLetters(number: number)
        let randomLetters = randomizeAvailableLetters(tileArraySize: amountOfRandomLetters)
        let rightAnswerLetters = getRightAnswerLetters(number: number)
        var letters = randomLetters + rightAnswerLetters
        letters.shuffle()
        for item in 0...letters.count-1{
            randomAndRightAnswerLetters.append(LetterModel(ans: letters[item], id: item))
        }
        return randomAndRightAnswerLetters
    }
}


