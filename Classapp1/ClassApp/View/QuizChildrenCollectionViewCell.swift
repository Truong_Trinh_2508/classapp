//
//  QuizChildrenCollectionViewCell.swift
//  ClassApp
//
//  Created by admin on 24/08/2021.
//

import UIKit
extension UIView {
    var parentShoesViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
class QuizChildrenCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgLocked: UIImageView!
    @IBOutlet weak var lbLocked: UILabel!
    @IBOutlet weak var imgContinue: UIImageView!
    @IBOutlet weak var lbPlayQuiz: UILabel!
    @IBOutlet weak var imgPlay: UIImageView!
    @IBOutlet weak var imgQuiz3: UIImageView!
    @IBOutlet weak var imgQuiz2: UIImageView!
    @IBOutlet weak var imgQuiz1: UIImageView!
    @IBOutlet weak var lbLevel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
