//
//  GuessChildrenCollectionViewCell.swift
//  ClassApp
//
//  Created by admin on 25/08/2021.
//

import UIKit

class GuessChildrenCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgLocked: UIImageView!
    @IBOutlet weak var lbLocked: UILabel!
    @IBOutlet weak var imgContinue: UIImageView!
    @IBOutlet weak var lbPlayGuess: UILabel!
    @IBOutlet weak var imgPlay: UIImageView!
    @IBOutlet weak var imgGuess3: UIImageView!
    @IBOutlet weak var imgGuess2: UIImageView!
    @IBOutlet weak var imgGuess1: UIImageView!
    @IBOutlet weak var lbLevelGuess: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
