//
//  LetterModel.swift
//  ClassApp
//
//  Created by admin on 28/09/2021.
//

import UIKit

class LetterModel: NSObject {
    var id: Int = 0
    var ans: String = ""
    
    init(ans: String, id: Int){
        self.ans = ans
        self.id = id
    }
}

