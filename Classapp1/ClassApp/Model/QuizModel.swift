//
//  QuizModel.swift
//  ClassApp
//
//  Created by admin on 26/08/2021.
//

import Foundation
import SQLite3

class QuizModel: NSObject {
    var id:Int = 0
    var ans1:String = ""
    var ans2:String = ""
    var ans3:String = ""
    var ques:String = ""
    var answer:String = ""
    init(id:Int, ans1:String, ans2: String, ans3: String, ques: String,answer:String)
    {
        self.id = id
        self.ans1 = ans1
        self.ans2 = ans2
        self.ans3 = ans3
        self.ques = ques
        self.answer = answer
    }
}
