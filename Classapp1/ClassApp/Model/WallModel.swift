//
//  WallModel.swift
//  ClassApp
//
//  Created by admin on 05/10/2021.
//

import Foundation
import SQLite.Swift

class WallModel: NSObject {
    var id:Int = 0
    var img:Blob = Blob(bytes: [0])
    init(id:Int, img :Blob){
        self.id = id
        self.img = img
    }
}
