//
//  GuessModel.swift
//  ClassApp
//
//  Created by admin on 26/08/2021.
//

import Foundation
import SQLite.Swift

class GuessModel:NSObject {
    var id:Int = 0
    var img:Blob = Blob(bytes: [0])
    var ans:String = ""
    init(id:Int, ans:String, img :Blob){
        self.id = id
        self.ans = ans
        self.img = img
    }

}
