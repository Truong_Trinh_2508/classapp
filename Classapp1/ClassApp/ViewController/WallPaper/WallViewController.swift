//
//  WallViewController.swift
//  ClassApp
//
//  Created by admin on 05/10/2021.
//

import UIKit

class WallViewController: UIViewController {
    var listDataWall:[WallModel] = [WallModel]()
    var numberWall = 0
    @IBOutlet weak var WallCollectionView: UICollectionView!
    @IBOutlet weak var Backgroud: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        WallCollectionView.register(UINib(nibName: "WallCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "WallCollectionViewCell")
        SqliteService.shared.getDataWall(){ repond,error in
            if let repond = repond{
                self.listDataWall = repond
                self.WallCollectionView.reloadData()
            }
        }    }

    @IBAction func btnActionBack(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        vc.modalPresentationStyle = .fullScreen 
        self.present(vc, animated: true)
    }
}
extension WallViewController : UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listDataWall.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WallCollectionViewCell", for: indexPath) as! WallCollectionViewCell
        if numberWall / 2 >= indexPath.item + 1 {
            let data = NSData(bytes:  listDataWall[indexPath.item].img.bytes, length: listDataWall[indexPath.item].img.bytes.count)
            if let image = UIImage(data: data as Data){
                cell.imgWall.image = image
            }
            cell.imgLocked.isHidden = true
            return cell
        }
        else{
            cell.imgWall.isHidden = true
            cell.imgLocked.isHidden = false
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if numberWall / 2 >= indexPath.item + 1 {
            switch indexPath.row  {
            case indexPath.row:
                let data = NSData(bytes:  listDataWall[indexPath.row].img.bytes, length: listDataWall[indexPath.row].img.bytes.count)
                if let image = UIImage(data: data as Data){
                    Backgroud.image = image
                }
            default:
                break
            }
        }
    }
}
extension WallViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 10.0, right: 0.0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 30
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 30
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width: UIScreen.main.bounds.width, height:  400)
        }
        return CGSize(width :280, height:  300)
    }
}
