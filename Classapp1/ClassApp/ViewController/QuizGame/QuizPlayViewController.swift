//
//  QuizPlayViewController.swift
//  ClassApp
//
//  Created by admin on 26/08/2021.
//

import UIKit
import AVFoundation
import SwiftKeychainWrapper

class QuizPlayViewController: UIViewController {
    var listDataGame = [QuizModel]()
    
    var Answer: String = ""
    var ans1:String = ""
    var ans2:String = ""
    var ans3:String = ""
    var ques:String = ""
    
    //valiable trung gian
    var answer:String = ""
    var check: Bool = true
    var numberquestionQuiz = 0
    var level = 0
    var sumOfQuestion = 5
    var isTrueAnswer = false
    var scoreQuiz:Int = 0
    var coin = 0
    
    
    @IBOutlet weak var Coin: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var lbLvQuesQuiz: UILabel!
    @IBOutlet weak var lbQuesQuiz: UILabel!
    @IBOutlet weak var viewQuesQuiz: UIView!
    @IBOutlet weak var lbLevelQuesQuiz: UILabel!
    
    @IBOutlet weak var lbAns1Quiz: UILabel!
    @IBOutlet weak var viewAns1Quiz: UIView!
    @IBOutlet weak var btnAns1Quiz: UIButton!
    
    @IBOutlet weak var lbAns2Quiz: UILabel!
    @IBOutlet weak var viewAns2Quiz: UIView!
    @IBOutlet weak var btnAns2Quiz: UIButton!
    
    @IBOutlet weak var lbAns3Quiz: UILabel!
    @IBOutlet weak var viewAns3Quiz: UIView!
    @IBOutlet weak var btnAns3Quiz: UIButton!
    
    @IBOutlet weak var btnHintQuiz: UIButton!
    @IBOutlet weak var btnNextQuiz: UIButton!
    @IBAction func backQuiz(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "QuizGameViewController") as! QuizGameViewController
        vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.present(vc, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getDataQuiz(){ _,_ in }
        dLogDebug(listDataGame)
        lbAns2Quiz.textColor = UIColor.white
        lbAns3Quiz.textColor = UIColor.white
        progressBar.layer.cornerRadius = 5
        progressBar.transform = progressBar.transform.scaledBy(x: 1, y: 2)
        btnNextQuiz.isUserInteractionEnabled = false
        Coin.text = String(coin)
        switch level {
        case 0:
            lbAns1Quiz.text = listDataGame[numberquestionQuiz].ans1
            lbAns2Quiz.text = listDataGame[numberquestionQuiz].ans2
            lbAns3Quiz.text = listDataGame[numberquestionQuiz].ans3
            lbQuesQuiz.text = listDataGame[numberquestionQuiz].ques
            Answer = listDataGame[numberquestionQuiz].answer
            lbLvQuesQuiz.text = String(numberquestionQuiz + 1) + "/40"

        case 1:
            lbAns1Quiz.text = listDataGame[numberquestionQuiz + 5].ans1
            lbAns2Quiz.text = listDataGame[numberquestionQuiz + 5].ans2
            lbAns3Quiz.text = listDataGame[numberquestionQuiz + 5].ans3
            lbQuesQuiz.text = listDataGame[numberquestionQuiz + 5].ques
            Answer = listDataGame[numberquestionQuiz + 5].answer
            lbLvQuesQuiz.text = String(numberquestionQuiz + 6) + "/40"

        case 2:
            lbAns1Quiz.text = listDataGame[numberquestionQuiz + 10].ans1
            lbAns2Quiz.text = listDataGame[numberquestionQuiz + 10].ans2
            lbAns3Quiz.text = listDataGame[numberquestionQuiz + 10].ans3
            lbQuesQuiz.text = listDataGame[numberquestionQuiz + 10].ques
            Answer = listDataGame[numberquestionQuiz + 10].answer
            lbLvQuesQuiz.text = String(numberquestionQuiz + 11) + "/40"

        case 3:
            lbAns1Quiz.text = listDataGame[numberquestionQuiz + 15].ans1
            lbAns2Quiz.text = listDataGame[numberquestionQuiz + 15].ans2
            lbAns3Quiz.text = listDataGame[numberquestionQuiz + 15].ans3
            lbQuesQuiz.text = listDataGame[numberquestionQuiz + 15].ques
            Answer = listDataGame[numberquestionQuiz + 15].answer
            lbLvQuesQuiz.text = String(numberquestionQuiz + 16) + "/40"

        case 4:
            lbAns1Quiz.text = listDataGame[numberquestionQuiz + 20].ans1
            lbAns2Quiz.text = listDataGame[numberquestionQuiz + 20].ans2
            lbAns3Quiz.text = listDataGame[numberquestionQuiz + 20].ans3
            lbQuesQuiz.text = listDataGame[numberquestionQuiz + 20].ques
            Answer = listDataGame[numberquestionQuiz + 20].answer
            lbLvQuesQuiz.text = String(numberquestionQuiz + 21) + "/40"

        case 5:
            lbAns1Quiz.text = listDataGame[numberquestionQuiz + 25].ans1
            lbAns2Quiz.text = listDataGame[numberquestionQuiz + 25].ans2
            lbAns3Quiz.text = listDataGame[numberquestionQuiz + 25].ans3
            lbQuesQuiz.text = listDataGame[numberquestionQuiz + 25].ques
            Answer = listDataGame[numberquestionQuiz + 25].answer
            lbLvQuesQuiz.text = String(numberquestionQuiz + 26) + "/40"

        case 6:
            lbAns1Quiz.text = listDataGame[numberquestionQuiz + 30].ans1
            lbAns2Quiz.text = listDataGame[numberquestionQuiz + 30].ans2
            lbAns3Quiz.text = listDataGame[numberquestionQuiz + 30].ans3
            lbQuesQuiz.text = listDataGame[numberquestionQuiz + 30].ques
            Answer = listDataGame[numberquestionQuiz + 30].answer
            lbLvQuesQuiz.text = String(numberquestionQuiz + 31) + "/40"

        case 7:
            lbAns1Quiz.text = listDataGame[numberquestionQuiz + 35].ans1
            lbAns2Quiz.text = listDataGame[numberquestionQuiz + 35].ans2
            lbAns3Quiz.text = listDataGame[numberquestionQuiz + 35].ans3
            lbQuesQuiz.text = listDataGame[numberquestionQuiz + 35].ques
            Answer = listDataGame[numberquestionQuiz + 35].answer
            lbLvQuesQuiz.text = String(numberquestionQuiz + 36) + "/40"
        default:
            break
        }
        if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil){
            KeychainWrapper.standard.set(0,forKey: "scoreQuiz")
            KeychainWrapper.standard.set(0,forKey: "coinQuiz")
            lbLevelQuesQuiz.text = "Score: 0/40"
            progressBar.progress = Float(0/40)
            Coin.text = "0"
        }
        else{
            if let completedQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz"){
                if let completedCoinQuiz = KeychainWrapper.standard.integer(forKey: "coinQuiz"){
                    lbLevelQuesQuiz.text = "Score: " + String(completedQuiz) + "/40"
                    progressBar.progress = Float(completedQuiz)/40.0
                    Coin.text = String(completedCoinQuiz/3)
                }
            }
            
            
        }
    }

    func getDataQuiz(andCompletion completion:@escaping(_ moviesResponse: [QuizModel], _ error: Error?) -> ()) {
        SqliteService.shared.getDataQuiz() { (response, error) in
            if let listData = response{
                self.listDataGame.removeAll()
                self.listDataGame = listData
                DispatchQueue.main.async {
                }
            }
            completion(self.listDataGame, error)
        }
    }
    
    //MARK:Btn A
    @IBAction func btnAns1(_ sender: Any) {
        if check == true{
            check = false
            lbAns1Quiz.textColor = .white
            btnNextQuiz.isUserInteractionEnabled = true
            btnAns1Quiz.isUserInteractionEnabled = false
            btnAns2Quiz.isUserInteractionEnabled = false
            btnAns3Quiz.isUserInteractionEnabled = false
            if lbAns1Quiz.text == Answer{
                isTrueAnswer = true
                scoreQuiz += 1
                coin += 1
//                lbLvQuesQuiz.text = String(scoreQuiz)
//                Coin.text = String(coin)
                KeychainWrapper.standard.remove(forKey: "coinQuiz")
                KeychainWrapper.standard.set(coin, forKey: "coinQuiz")
                KeychainWrapper.standard.remove(forKey: "scoreQuiz")
                KeychainWrapper.standard.set(scoreQuiz, forKey: "scoreQuiz")
                if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil){
                    KeychainWrapper.standard.set(0,forKey: "scoreQuiz")
                    KeychainWrapper.standard.set(0,forKey: "coinQuiz")
                    lbLevelQuesQuiz.text = "Score: 0/40"
                    progressBar.progress = Float(0/40)
                    Coin.text = "0"
                }
                else{
                    if let completedQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz"){
                        if  let completedCoinQuiz = KeychainWrapper.standard.integer(forKey: "coinQuiz"){
                            lbLevelQuesQuiz.text = "Score: " + String(completedQuiz) + "/40"
                            progressBar.progress = Float(completedQuiz)/40.0
                            Coin.text = String(completedCoinQuiz/3)
                        }
                    }
                    
                    
                }
                viewAns1Quiz.backgroundColor = #colorLiteral(red: 0.7269740701, green: 0.9956488013, blue: 0.654661119, alpha: 1)
                if scoreQuiz % 3 == 0 {
                    let vc = storyboard?.instantiateViewController(identifier: "WinViewController") as! WinViewController
                    vc.modalPresentationStyle = .fullScreen
                    present(vc, animated: true, completion: nil)
                }
                
            }
            else{
                isTrueAnswer = false
                viewAns1Quiz.backgroundColor = #colorLiteral(red: 0.9915545583, green: 0.7857278585, blue: 0.7826460004, alpha: 1)
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "LoseQuizViewController") as! LoseQuizViewController
                vc.modalPresentationStyle = .fullScreen
                present(vc, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: btn B
    @IBAction func btnAns2(_ sender: Any) {
        if check == true{
            check = false
            lbAns2Quiz.textColor = .white
            btnNextQuiz.isUserInteractionEnabled = true
            btnAns1Quiz.isUserInteractionEnabled = false
            btnAns2Quiz.isUserInteractionEnabled = false
            btnAns3Quiz.isUserInteractionEnabled = false
            if lbAns2Quiz.text == Answer{
                isTrueAnswer = true
                scoreQuiz += 1
                coin += 1
                lbLevelQuesQuiz.text = String(scoreQuiz)
                Coin.text = String(coin)
                KeychainWrapper.standard.remove(forKey: "coinQuiz")
                KeychainWrapper.standard.set(coin, forKey: "coinQuiz")
                KeychainWrapper.standard.remove(forKey: "scoreQuiz")
                KeychainWrapper.standard.set(scoreQuiz, forKey: "scoreQuiz")
                if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil){
                    KeychainWrapper.standard.set(0,forKey: "scoreQuiz")
                    KeychainWrapper.standard.set(0,forKey: "coinQuiz")
                    lbLevelQuesQuiz.text = "Score: 0/40"
                    progressBar.progress = Float(0/40)
                    Coin.text = "0"
                }
                else{
                    if let completedQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz"){
                        if let completedCoinQuiz = KeychainWrapper.standard.integer(forKey: "coinQuiz"){
                            lbLevelQuesQuiz.text = "Score: " + String(completedQuiz) + "/40"
                            progressBar.progress = Float(completedQuiz)/40.0
                            Coin.text = String(completedCoinQuiz/3)
                        }
                    }
                }
                viewAns2Quiz.backgroundColor = #colorLiteral(red: 0.7269740701, green: 0.9956488013, blue: 0.654661119, alpha: 1)
                if scoreQuiz % 3 == 0 {
                    let vc = storyboard?.instantiateViewController(identifier: "WinViewController") as! WinViewController
                    vc.modalPresentationStyle = .fullScreen
                    present(vc, animated: true, completion: nil)
                }
                
            }
            else{
                isTrueAnswer = false
                viewAns2Quiz.backgroundColor = #colorLiteral(red: 0.9915545583, green: 0.7857278585, blue: 0.7826460004, alpha: 1)
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "LoseQuizViewController") as! LoseQuizViewController
                vc.modalPresentationStyle = .fullScreen
                present(vc, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: btn C
    @IBAction func btnAns3(_ sender: Any) {
        if check == true{
            check = false
            lbAns3Quiz.textColor = .white
            btnNextQuiz.isUserInteractionEnabled = true
            btnAns1Quiz.isUserInteractionEnabled = false
            btnAns2Quiz.isUserInteractionEnabled = false
            btnAns3Quiz.isUserInteractionEnabled = false
            if lbAns3Quiz.text == Answer{
                isTrueAnswer = true
                scoreQuiz += 1
                coin += 1
//                lbLevelQuesQuiz.text = String(scoreQuiz)
//                Coin.text = String(coin)
                KeychainWrapper.standard.remove(forKey: "coinQuiz")
                KeychainWrapper.standard.set(coin, forKey: "coinQuiz")
                KeychainWrapper.standard.remove(forKey: "scoreQuiz")
                KeychainWrapper.standard.set(scoreQuiz, forKey: "scoreQuiz")
                if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil){
                    KeychainWrapper.standard.set(0,forKey: "scoreQuiz")
                    KeychainWrapper.standard.set(0,forKey: "coinQuiz")
                    lbLevelQuesQuiz.text = "Score: 0/40"
                    progressBar.progress = Float(0/40)
                    Coin.text = "0"
                }
                else{
                    if let completedQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz"){
                        if let completedCoinQuiz = KeychainWrapper.standard.integer(forKey: "coinQuiz"){
                            lbLevelQuesQuiz.text = "Score: " + String(completedQuiz) + "/40"
                            progressBar.progress = Float(completedQuiz)/40.0
                            Coin.text = String(completedCoinQuiz/3)
                        }
                        
                    }
                    
                }
                viewAns3Quiz.backgroundColor = #colorLiteral(red: 0.7269740701, green: 0.9956488013, blue: 0.654661119, alpha: 1)
                if scoreQuiz % 3 == 0 {
                    let vc = storyboard?.instantiateViewController(identifier: "WinViewController") as! WinViewController
                    vc.modalPresentationStyle = .fullScreen
                    present(vc, animated: true, completion: nil)
                }
            }
            else{
                isTrueAnswer = false
                viewAns3Quiz.backgroundColor = #colorLiteral(red: 0.9915545583, green: 0.7857278585, blue: 0.7826460004, alpha: 1)
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "LoseQuizViewController") as! LoseQuizViewController
                vc.modalPresentationStyle = .fullScreen
                present(vc, animated: true, completion: nil)
            }
        }
    }
    @IBAction func btnNextQuiz(_ sender: Any) {
 
        if (numberquestionQuiz + 1) == sumOfQuestion {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "QuizGameViewController") as! QuizGameViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true)
        }
        else
        {
            if isTrueAnswer {
                numberquestionQuiz += 1
            }
            else
            {
                numberquestionQuiz += 0
            }
            lbLvQuesQuiz.text = String(numberquestionQuiz + 1) + "/40"
            switch level {
            case 0:
                KeychainWrapper.standard.remove(forKey: "levelQuiz")
                KeychainWrapper.standard.set(numberquestionQuiz, forKey: "levelQuiz")
            case 1:
                KeychainWrapper.standard.remove(forKey: "levelQuiz")
                KeychainWrapper.standard.set(numberquestionQuiz, forKey: "levelQuiz")
            case 2:
                KeychainWrapper.standard.remove(forKey: "levelQuiz")
                KeychainWrapper.standard.set(numberquestionQuiz, forKey: "levelQuiz")
            case 3:
                KeychainWrapper.standard.remove(forKey: "levelQuiz")
                KeychainWrapper.standard.set(numberquestionQuiz, forKey: "levelQuiz")
            case 4:
                KeychainWrapper.standard.remove(forKey: "levelQuiz")
                KeychainWrapper.standard.set(numberquestionQuiz, forKey: "levelQuiz")
            case 5:
                KeychainWrapper.standard.remove(forKey: "levelQuiz")
                KeychainWrapper.standard.set(numberquestionQuiz, forKey: "levelQuiz")
            case 6:
                KeychainWrapper.standard.remove(forKey: "levelQuiz")
                KeychainWrapper.standard.set(numberquestionQuiz, forKey: "levelQuiz")
            case 7:
                KeychainWrapper.standard.remove(forKey: "levelQuiz")
                KeychainWrapper.standard.set(numberquestionQuiz, forKey: "levelQuiz")
            default:
                break
            }
            check = true
            btnAns1Quiz.isUserInteractionEnabled = true
            btnAns2Quiz.isUserInteractionEnabled = true
            btnAns3Quiz.isUserInteractionEnabled = true
            btnNextQuiz.isUserInteractionEnabled = false
            viewAns1Quiz.backgroundColor = UIColor.clear
            viewAns2Quiz.backgroundColor = UIColor.clear
            viewAns3Quiz.backgroundColor = UIColor.clear
            lbAns1Quiz.textColor = UIColor.white
            lbAns2Quiz.textColor = UIColor.white
            lbAns3Quiz.textColor = UIColor.white
            
            
            switch level {
            case 0:
                lbAns1Quiz.text = listDataGame[numberquestionQuiz].ans1
                lbAns2Quiz.text = listDataGame[numberquestionQuiz].ans2
                lbAns3Quiz.text = listDataGame[numberquestionQuiz].ans3
                lbQuesQuiz.text = listDataGame[numberquestionQuiz].ques
                Answer = listDataGame[numberquestionQuiz].answer
                lbLvQuesQuiz.text = String(numberquestionQuiz + 1) + "/40"

            case 1:
                lbAns1Quiz.text = listDataGame[numberquestionQuiz + 5].ans1
                lbAns2Quiz.text = listDataGame[numberquestionQuiz + 5].ans2
                lbAns3Quiz.text = listDataGame[numberquestionQuiz + 5].ans3
                lbQuesQuiz.text = listDataGame[numberquestionQuiz + 5].ques
                Answer = listDataGame[numberquestionQuiz + 5].answer
                lbLvQuesQuiz.text = String(numberquestionQuiz + 6) + "/40"

            case 2:
                lbAns1Quiz.text = listDataGame[numberquestionQuiz + 10].ans1
                lbAns2Quiz.text = listDataGame[numberquestionQuiz + 10].ans2
                lbAns3Quiz.text = listDataGame[numberquestionQuiz + 10].ans3
                lbQuesQuiz.text = listDataGame[numberquestionQuiz + 10].ques
                Answer = listDataGame[numberquestionQuiz + 10].answer
                lbLvQuesQuiz.text = String(numberquestionQuiz + 11) + "/40"

            case 3:
                lbAns1Quiz.text = listDataGame[numberquestionQuiz + 15].ans1
                lbAns2Quiz.text = listDataGame[numberquestionQuiz + 15].ans2
                lbAns3Quiz.text = listDataGame[numberquestionQuiz + 15].ans3
                lbQuesQuiz.text = listDataGame[numberquestionQuiz + 15].ques
                Answer = listDataGame[numberquestionQuiz + 15].answer
                lbLvQuesQuiz.text = String(numberquestionQuiz + 16) + "/40"

            case 4:
                lbAns1Quiz.text = listDataGame[numberquestionQuiz + 20].ans1
                lbAns2Quiz.text = listDataGame[numberquestionQuiz + 20].ans2
                lbAns3Quiz.text = listDataGame[numberquestionQuiz + 20].ans3
                lbQuesQuiz.text = listDataGame[numberquestionQuiz + 20].ques
                Answer = listDataGame[numberquestionQuiz + 20].answer
                lbLvQuesQuiz.text = String(numberquestionQuiz + 21) + "/40"

            case 5:
                lbAns1Quiz.text = listDataGame[numberquestionQuiz + 25].ans1
                lbAns2Quiz.text = listDataGame[numberquestionQuiz + 25].ans2
                lbAns3Quiz.text = listDataGame[numberquestionQuiz + 25].ans3
                lbQuesQuiz.text = listDataGame[numberquestionQuiz + 25].ques
                Answer = listDataGame[numberquestionQuiz + 25].answer
                lbLvQuesQuiz.text = String(numberquestionQuiz + 26) + "/40"

            case 6:
                lbAns1Quiz.text = listDataGame[numberquestionQuiz + 30].ans1
                lbAns2Quiz.text = listDataGame[numberquestionQuiz + 30].ans2
                lbAns3Quiz.text = listDataGame[numberquestionQuiz + 30].ans3
                lbQuesQuiz.text = listDataGame[numberquestionQuiz + 30].ques
                Answer = listDataGame[numberquestionQuiz + 30].answer
                lbLvQuesQuiz.text = String(numberquestionQuiz + 31) + "/40"

            case 7:
                lbAns1Quiz.text = listDataGame[numberquestionQuiz + 35].ans1
                lbAns2Quiz.text = listDataGame[numberquestionQuiz + 35].ans2
                lbAns3Quiz.text = listDataGame[numberquestionQuiz + 35].ans3
                lbQuesQuiz.text = listDataGame[numberquestionQuiz + 35].ques
                Answer = listDataGame[numberquestionQuiz + 35].answer
                lbLvQuesQuiz.text = String(numberquestionQuiz + 36) + "/40"
            default:
                break
            }
            
        }
    }
    
}

