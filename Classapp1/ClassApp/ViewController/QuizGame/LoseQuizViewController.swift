//
//  LoseQuizViewController.swift
//  ClassApp
//
//  Created by admin on 04/10/2021.
//

import UIKit

class LoseQuizViewController: UIViewController {

    var numberQuestion = 0
    var coin = 0
    var score = 0
    
    @IBOutlet weak var btnPlayQuizAgain: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func BtnPlayQuizAgain(_ sender: Any) {
//        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "GuessPlayViewController") as! GuessPlayViewController
//        vc.numberQuestion = numberQuestion
//        vc.coin += coin
//        vc.score += score
//        vc.modalPresentationStyle = .overFullScreen
//        vc.modalTransitionStyle = .crossDissolve //or .overFullScreen for transparency
//        self.present(vc, animated: true, completion: nil)
        self.dismiss(animated: true)
    }
}
