//
//  QuizGameViewController.swift
//  ClassApp
//
//  Created by admin on 24/08/2021.
//
import UIKit
import SwiftKeychainWrapper

class QuizGameViewController: UIViewController {
    
    var listQuiz:[QuizModel] = [QuizModel]()
    var listDataGame:[QuizModel] = [QuizModel]()
    var numberquestion = 0
    var score = 0
    var coin = 0
    
    @IBOutlet weak var lbLevelQuizGame: UILabel!
    @IBOutlet weak var ProgessBarQuiz: UIProgressView!
    @IBOutlet weak var CoinQuiz: UILabel!
    @IBOutlet weak var quizCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        ProgessBarQuiz.layer.cornerRadius = 5
        ProgessBarQuiz.transform = ProgessBarQuiz.transform.scaledBy(x: 1, y: 2)
        quizCollectionView.delegate = self
        quizCollectionView.dataSource = self
        quizCollectionView.register(UINib(nibName: QuizChildrenCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: QuizChildrenCollectionViewCell.className)
        SqliteService.shared.getDataQuiz(){ repond,error in
            if let repond = repond{
                self.listQuiz = repond
                self.quizCollectionView.reloadData()
            }
        }
        if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil){
            lbLevelQuizGame.text = "Score: 0/40"
            ProgessBarQuiz.progress = Float(0/40)
            CoinQuiz.text = "0"
        }
        else{
            let completedQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz")
            let completedCoinQuiz = KeychainWrapper.standard.integer(forKey: "coinQuiz")
            lbLevelQuizGame.text = "Score: " + String(completedQuiz!) + "/40"
            ProgessBarQuiz.progress = Float(completedQuiz!)/40.0
            if let completedCoinQuiz = completedCoinQuiz{
                CoinQuiz.text = String(completedCoinQuiz/3)
            }
            
        }
        if KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil {
            KeychainWrapper.standard.set(0, forKey: "scoreQuiz")
        }
        score = KeychainWrapper.standard.integer(forKey: "scoreQuiz")!
        if KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil {
            KeychainWrapper.standard.set(0, forKey: "coinQuiz")
        }
        coin = KeychainWrapper.standard.integer(forKey: "coinQuiz")!
        
    }
    
    @IBAction func backMain(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.present(vc, animated: true)
    }
}
extension QuizGameViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "QuizPlayViewController") as! QuizPlayViewController
        vc.level = indexPath.item
        switch vc.level {
        case 0:
            if KeychainWrapper.standard.integer(forKey: "levelQuiz") == nil{
                if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil){
                    KeychainWrapper.standard.set(0, forKey: "scoreQuiz")
                    KeychainWrapper.standard.set(0, forKey: "coinQuiz")
                }
                KeychainWrapper.standard.set(0, forKey: "levelQuiz")
                vc.numberquestionQuiz = KeychainWrapper.standard.integer(forKey: "levelQuiz")!
                vc.scoreQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz")!
                vc.coin = KeychainWrapper.standard.integer(forKey: "coinQuiz")!
            }
            else{
                let numberQuizQues = KeychainWrapper.standard.integer(forKey: "levelQuiz")!
                if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil){
                    KeychainWrapper.standard.set(0, forKey: "scoreQuiz")
                    KeychainWrapper.standard.set(0, forKey: "coinQuiz")
                }
                vc.scoreQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz")!
                vc.coin = KeychainWrapper.standard.integer(forKey: "coinQuiz")!
                if numberquestion >= 5 {
                    vc.numberquestionQuiz = 0
                    vc.scoreQuiz -= 5
                }
                else{
                    vc.numberquestionQuiz = numberQuizQues
                }
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true)
            }

        case 1:
            if score >= 5{
                if KeychainWrapper.standard.integer(forKey: "levelQuiz") == nil{
                    if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreQuiz")
                        KeychainWrapper.standard.set(0, forKey: "coinQuiz")
                    }
                    KeychainWrapper.standard.set(0, forKey: "levelQuiz")
                    vc.numberquestionQuiz = KeychainWrapper.standard.integer(forKey: "levelQuiz")!
                    vc.scoreQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz")!
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinQuiz")!
                }
                else{
                    let numberQuizQues = KeychainWrapper.standard.integer(forKey: "levelQuiz")!
                    if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreQuiz")
                        KeychainWrapper.standard.set(0, forKey: "coinQuiz")
                    }
                    vc.scoreQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz")!
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinQuiz")!
                    if numberquestion >= 10 {
                        vc.numberquestionQuiz = 5
                        vc.scoreQuiz -= 5
                    }
                    else{
                        vc.numberquestionQuiz = numberQuizQues
                    }
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true)
                }
            }
        case 2:
            if score >= 10 {
                if KeychainWrapper.standard.integer(forKey: "levelQuiz") == nil{
                    if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreQuiz")
                        KeychainWrapper.standard.set(0, forKey: "coinQuiz")
                    }
                    KeychainWrapper.standard.set(0, forKey: "levelQuiz")
                    vc.numberquestionQuiz = KeychainWrapper.standard.integer(forKey: "levelQuiz")!
                    vc.scoreQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz")!
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinQuiz")!
                }
                else{
                    let numberQuizQues = KeychainWrapper.standard.integer(forKey: "levelQuiz")!
                    if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreQuiz")
                        KeychainWrapper.standard.set(0, forKey: "coinQuiz")
                    }
                    vc.scoreQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz")!
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinQuiz")!
                    if numberQuizQues >= 15 {
                        vc.numberquestionQuiz = 10
                        vc.scoreQuiz -= 5
                    }
                    else{
                        vc.numberquestionQuiz = numberQuizQues
                    }
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true)
                }
            }
        case 3:
            if score >= 15 {
                if KeychainWrapper.standard.integer(forKey: "levelQuiz") == nil{
                    if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreQuiz")
                        KeychainWrapper.standard.set(0, forKey: "coinQuiz")
                    }
                    KeychainWrapper.standard.set(0, forKey: "levelQuiz")
                    vc.numberquestionQuiz = KeychainWrapper.standard.integer(forKey: "levelQuiz")!
                    vc.scoreQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz")!
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinQuiz")!
                }
                else{
                    let numberQuizQues = KeychainWrapper.standard.integer(forKey: "levelQuiz")!
                    if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreQuiz")
                        KeychainWrapper.standard.set(0, forKey: "coinQuiz")
                    }
                    vc.scoreQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz")!
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinQuiz")!
                    if numberQuizQues >= 20 {
                        vc.numberquestionQuiz = 15
                        vc.scoreQuiz -= 5
                    }
                    else{
                        vc.numberquestionQuiz = numberQuizQues
                    }
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true)
                }
            }
        case 4:
            if score >= 20 {
                if KeychainWrapper.standard.integer(forKey: "levelQuiz") == nil{
                    if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreQuiz")
                        KeychainWrapper.standard.set(0, forKey: "coinQuiz")
                    }
                    KeychainWrapper.standard.set(0, forKey: "levelQuiz")
                    vc.numberquestionQuiz = KeychainWrapper.standard.integer(forKey: "levelQuiz")!
                    vc.scoreQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz")!
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinQuiz")!
                }
                else{
                    let numberQuizQues = KeychainWrapper.standard.integer(forKey: "levelQuiz")!
                    if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreQuiz")
                        KeychainWrapper.standard.set(0, forKey: "coinQuiz")
                    }
                    vc.scoreQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz")!
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinQuiz")!
                    if numberQuizQues >= 25 {
                        vc.numberquestionQuiz = 20
                        vc.scoreQuiz -= 5
                    }
                    else{
                        vc.numberquestionQuiz = numberQuizQues
                    }
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true)
                }
            }
        case 5:
            if score >= 25 {
                if KeychainWrapper.standard.integer(forKey: "levelQuiz") == nil{
                    if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreQuiz")
                        KeychainWrapper.standard.set(0, forKey: "coinQuiz")
                    }
                    KeychainWrapper.standard.set(0, forKey: "levelQuiz")
                    vc.numberquestionQuiz = KeychainWrapper.standard.integer(forKey: "levelQuiz")!
                    vc.scoreQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz")!
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinQuiz")!
                }
                else{
                    let numberQuizQues = KeychainWrapper.standard.integer(forKey: "levelQuiz")!
                    if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreQuiz")
                        KeychainWrapper.standard.set(0, forKey: "coinQuiz")
                    }
                    vc.scoreQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz")!
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinQuiz")!
                    if numberQuizQues >= 30 {
                        vc.numberquestionQuiz = 25
                        vc.scoreQuiz -= 5
                    }
                    else{
                        vc.numberquestionQuiz = numberQuizQues
                    }
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true)
                }
            }
        case 6:
            if score >= 30 {
                if KeychainWrapper.standard.integer(forKey: "levelQuiz") == nil{
                    if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreQuiz")
                        KeychainWrapper.standard.set(0, forKey: "coinQuiz")
                    }
                    KeychainWrapper.standard.set(0, forKey: "levelQuiz")
                    vc.numberquestionQuiz = KeychainWrapper.standard.integer(forKey: "levelQuiz")!
                    vc.scoreQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz")!
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinQuiz")!
                }
                else{
                    let numberQuizQues = KeychainWrapper.standard.integer(forKey: "levelQuiz")!
                    if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreQuiz")
                        KeychainWrapper.standard.set(0, forKey: "coinQuiz")
                    }
                    vc.scoreQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz")!
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinQuiz")!
                    if numberQuizQues >= 35 {
                        vc.numberquestionQuiz = 30
                        vc.scoreQuiz -= 5
                    }
                    else{
                        vc.numberquestionQuiz = numberQuizQues
                    }
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true)
                }
            }
        case 7:
            if score >= 35 {
                if KeychainWrapper.standard.integer(forKey: "levelQuiz") == nil{
                    if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreQuiz")
                        KeychainWrapper.standard.set(0, forKey: "coinQuiz")
                    }
                    KeychainWrapper.standard.set(0, forKey: "levelQuiz")
                    vc.numberquestionQuiz = KeychainWrapper.standard.integer(forKey: "levelQuiz")!
                    vc.scoreQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz")!
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinQuiz")!
                }
                else{
                    let numberQuizQues = KeychainWrapper.standard.integer(forKey: "levelQuiz")!
                    if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreQuiz")
                        KeychainWrapper.standard.set(0, forKey: "coinQuiz")
                    }
                    vc.scoreQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz")!
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinQuiz")!
                    if numberQuizQues >= 40 {
                        vc.numberquestionQuiz = 35
                        vc.scoreQuiz -= 5
                    }
                    else{
                        vc.numberquestionQuiz = numberQuizQues
                    }
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true)
                }
            }
        default:
            break
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = quizCollectionView.dequeueReusableCell(withReuseIdentifier: QuizChildrenCollectionViewCell.className, for: indexPath) as! QuizChildrenCollectionViewCell
        cell.imgContinue.isHidden = true
        cell.imgQuiz3.isHidden = true
        cell.imgQuiz2.isHidden = true
        cell.imgQuiz1.isHidden = true
        cell.imgPlay.isHidden = true
        cell.imgLocked.isHidden = true
        cell.lbLocked.isHidden = true
        cell.lbPlayQuiz.isHidden = true
        
        switch indexPath.item {
        case 0:
            cell.imgQuiz3.isHidden = false
            cell.imgQuiz2.isHidden = false
            cell.imgQuiz1.isHidden = false
            cell.imgPlay.isHidden = false
            cell.lbPlayQuiz.isHidden = false
            cell.lbLevel.text = "Level 1"
        case 1:
            cell.lbLevel.text = "Level 2"
            if score >= 5 {
                cell.imgQuiz1.isHidden = false
                cell.imgQuiz2.isHidden = false
                cell.imgQuiz3.isHidden = false
                cell.imgPlay.isHidden = false
                cell.lbPlayQuiz.isHidden = false
                
            }
            else
            {
                cell.imgContinue.isHidden = false
                cell.imgLocked.isHidden = false
                cell.lbLocked.isHidden = false
            }
        case 2:
            cell.lbLevel.text = "Level 3"
            if score >= 10 {
                cell.imgQuiz1.isHidden = false
                cell.imgQuiz2.isHidden = false
                cell.imgQuiz3.isHidden = false
                cell.imgPlay.isHidden = false
                cell.lbPlayQuiz.isHidden = false
            }
            else
            {
                cell.imgContinue.isHidden = false
                cell.imgLocked.isHidden = false
                cell.lbLocked.isHidden = false
            }
        case 3:
            cell.lbLevel.text = "Level 4"
            if score >= 15 {
                cell.imgQuiz1.isHidden = false
                cell.imgQuiz2.isHidden = false
                cell.imgQuiz3.isHidden = false
                cell.imgPlay.isHidden = false
                cell.lbPlayQuiz.isHidden = false
            }
            else
            {
                cell.imgContinue.isHidden = false
                cell.imgLocked.isHidden = false
                cell.lbLocked.isHidden = false
            }
        case 4:
            cell.lbLevel.text = "Level 5"
            if score >= 20 {
                cell.imgQuiz1.isHidden = false
                cell.imgQuiz2.isHidden = false
                cell.imgQuiz3.isHidden = false
                cell.imgPlay.isHidden = false
                cell.lbPlayQuiz.isHidden = false
            }
            else
            {
                cell.imgContinue.isHidden = false
                cell.imgLocked.isHidden = false
                cell.lbLocked.isHidden = false
            }
        case 5:
            cell.lbLevel.text = "Level 6"
            if score >= 25 {
                cell.imgQuiz1.isHidden = false
                cell.imgQuiz2.isHidden = false
                cell.imgQuiz3.isHidden = false
                cell.imgPlay.isHidden = false
                cell.lbPlayQuiz.isHidden = false
            }
            else
            {
                cell.imgContinue.isHidden = false
                cell.imgLocked.isHidden = false
                cell.lbLocked.isHidden = false
            }
        case 6:
            cell.lbLevel.text = "Level 7"
            if score >= 30 {
                cell.imgQuiz1.isHidden = false
                cell.imgQuiz2.isHidden = false
                cell.imgQuiz3.isHidden = false
                cell.imgPlay.isHidden = false
                cell.lbPlayQuiz.isHidden = false
            }
            else
            {
                cell.imgContinue.isHidden = false
                cell.imgLocked.isHidden = false
                cell.lbLocked.isHidden = false
            }
        case 7:
            cell.lbLevel.text = "Level 8"
            if score >= 35 {
                cell.imgQuiz1.isHidden = false
                cell.imgQuiz2.isHidden = false
                cell.imgQuiz3.isHidden = false
                cell.imgPlay.isHidden = false
                cell.lbPlayQuiz.isHidden = false
            }
            else
            {
                cell.imgContinue.isHidden = false
                cell.imgLocked.isHidden = false
                cell.lbLocked.isHidden = false
            }
        default:
            break
        }
        return cell
    }
    
}
extension QuizGameViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width: UIScreen.main.bounds.width/2-10, height:  300)
        }
        return CGSize(width: UIScreen.main.bounds.width/2-10, height:  200)
    }
    
}
