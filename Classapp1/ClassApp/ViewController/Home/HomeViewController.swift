//
//  ViewController.swift
//  ClassApp
//
//  Created by admin on 21/08/2021.
//
import VHProgressBar
import UIKit
import SwiftKeychainWrapper
extension NSObject {
    func background(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }
    var className: String {
        return String(describing: type(of: self))
    }
    class var className: String {
        return String(describing: self)
    }
}
class HomeViewController: UIViewController {

    @IBOutlet weak var MainCoin: UILabel!
    @IBOutlet weak var lbLvWall: UILabel!
    @IBOutlet weak var viewQuiz: UIView!
    @IBOutlet weak var viewGuess: UIView!
    @IBOutlet weak var viewWall: UIView!
    @IBOutlet weak var WallPaper: UIButton!
    @IBOutlet weak var GuessName: UIButton!
    @IBOutlet weak var QuizGame: UIButton!
    @IBOutlet weak var prgGuess: UIProgressView!
    @IBOutlet weak var prgQuiz: UIProgressView!
    
    @IBOutlet weak var lbLvMainQuiz: UILabel!
    @IBOutlet weak var ProgessMainQuiz: UIProgressView!
    @IBOutlet weak var lbLvMainGuess: UILabel!
    @IBOutlet weak var ProgessMainGuess: UIProgressView!
    override func viewDidLoad() {
        super.viewDidLoad()
        prgGuess.layer.cornerRadius = 5
        prgGuess.transform = prgGuess.transform.scaledBy(x: 1, y: 2)
        prgQuiz.layer.cornerRadius = 5
        prgQuiz.transform = prgQuiz.transform.scaledBy(x: 1, y: 2)

        //Score Quiz
        if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil){
            lbLvMainQuiz.text = "Score: 0/40"
            ProgessMainQuiz.progress = Float(0/40)
        }
        else{
            let completedQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz")
            lbLvMainQuiz.text = "Score: " + String(completedQuiz!) + "/40"
            ProgessMainQuiz.progress = Float(completedQuiz!)/40.0
        }
        //Score Guess
        if (KeychainWrapper.standard.integer(forKey: "levelGuess") == nil) {
            MainCoin.text = "0"
            lbLvMainGuess.text = "Score: 0/30"
            prgGuess.progress = 0
        }
        else{
            let Completed = KeychainWrapper.standard.integer(forKey: "levelGuess")
            MainCoin.text = String(Completed!)
            lbLvMainGuess.text = "Score: " + String(Completed!) + " /30"
            prgGuess.progress = Float(Completed!)/30.0
        }
        //SCore Wall

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if (KeychainWrapper.standard.integer(forKey: "coinGuess") == nil && KeychainWrapper.standard.integer(forKey: "coinQuiz") == nil) {
            KeychainWrapper.standard.set(0, forKey: "coinGuess")
            KeychainWrapper.standard.set(0, forKey: "coinQuiz")
            MainCoin.text = "0"
        }
        else{
            let CompletedCoinGuess = KeychainWrapper.standard.integer(forKey: "coinGuess")
            let completedCoinQuiz = KeychainWrapper.standard.integer(forKey: "coinQuiz")
            MainCoin.text = String( CompletedCoinGuess! + completedCoinQuiz!/3)
        }
        if (KeychainWrapper.standard.integer(forKey: "scoreQuiz") == nil && KeychainWrapper.standard.integer(forKey: "scoreGuess") == nil){
            KeychainWrapper.standard.set(0, forKey: "scoreQuiz")
            KeychainWrapper.standard.set(0, forKey: "scoreGuess")
            lbLvWall.text = "0"
        }
        else{
            let completedQuiz = KeychainWrapper.standard.integer(forKey: "scoreQuiz")
            let completedGuess = KeychainWrapper.standard.integer(forKey: "scoreGuess")
            lbLvWall.text = String(completedQuiz! + completedGuess!) + "/70"
        }

    }


    
    @IBAction func btnQuizGame(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(identifier: "QuizGameViewController") as! QuizGameViewController

        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }

    @IBAction func btnGuessName(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(identifier: "GuessNameViewController") as! GuessNameViewController
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnWallPaper(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(identifier: "WallViewController") as! WallViewController
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
}

