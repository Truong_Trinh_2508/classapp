//
//  LaucherViewController.swift
//  ClassApp
//
//  Created by admin on 09/09/2021.
//

import UIKit

class LaucherViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        var timecout = 0
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true){ timer in
            timecout = timecout + 1
            if timecout == 2 {
                let vc = self.storyboard?.instantiateViewController(identifier: "HomeViewController") as! HomeViewController
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
                timer.invalidate()
            }
        }
    }

}
