//
//  LoseViewController.swift
//  ClassApp
//
//  Created by admin on 03/09/2021.
//

import UIKit

class LoseGuessViewController: UIViewController {
    var numberQuestion = 0
    var coin = 0
    var score = 0
    
    @IBOutlet weak var btnPlayGuessAgain: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btnPlayGuessAgain(_ sender: Any) {
        if numberQuestion == 5 {
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "GuessNameViewController") as! GuessNameViewController
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve //or .overFullScreen for transparency
            self.present(vc, animated: true, completion: nil)
        }
        else
        {
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "GuessPlayViewController") as! GuessPlayViewController
            vc.numberQuestion = numberQuestion
            vc.coin += coin
            vc.score += score
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve //or .overFullScreen for transparency
            self.present(vc, animated: true, completion: nil)
        }
    }
}
