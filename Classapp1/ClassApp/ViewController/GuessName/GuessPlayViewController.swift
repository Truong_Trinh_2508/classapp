//
//  GuessPlayViewController.swift
//  ClassApp
//
//  Created by admin on 26/08/2021.
//

import UIKit
import Kingfisher
import SwiftKeychainWrapper


class GuessPlayViewController: UIViewController {
    
    @IBOutlet weak var progressBarGuess: UIProgressView!
    @IBOutlet weak var lbLvMainGuess: UILabel!
    @IBOutlet weak var CoinGuess: UILabel!
    @IBOutlet weak var lbLvGuessName: UILabel!
    @IBOutlet weak var imgGuessName: UIImageView!
    @IBOutlet weak var AnsCollectionView: UICollectionView!
    @IBOutlet weak var guessCollectionView: UICollectionView!
    static let shared: GuessPlayViewController = GuessPlayViewController()
    var isMuteSound = false
    var isMuteMusic = false
    var coin = 0
    var display = false
    var image: UIImage!
    var numberQuestion = 0
    var listDataGuess: [GuessModel] = [GuessModel]()
    var listDataGuess_filter: [GuessModel] = [GuessModel]()
    var listLetter: [LetterModel] = [LetterModel]()
    var listLetter_filter: [LetterModel] = [LetterModel]()
    var listLettersOfRightAnswer: [String] = []
    var letter = ""
    var number = 0
    var listNumber: [Int] = []
    var letterArr: [String] = []
    var score = 0
    var level = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        AnsCollectionView.register(UINib(nibName: "TextAnsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TextAnsCollectionViewCell")
        guessCollectionView.register(UINib(nibName: "TextAnsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TextAnsCollectionViewCell")
        listDataGuess = SqliteService.shared.listDataGuess
        listLetter = SqliteService.shared.shuffleLetters(number: numberQuestion + 1)
        let data = NSData(bytes:  listDataGuess[numberQuestion].img.bytes, length: listDataGuess[numberQuestion].img.bytes.count)
        if let image = UIImage(data: data as Data){
            imgGuessName.image = image
        }
        if (KeychainWrapper.standard.integer(forKey: "levelGuess") == nil) {
            CoinGuess.text = "0"
            lbLvMainGuess.text = "Score: 0/30"
            progressBarGuess.progress = 0
        }
        else{
            let Completed = KeychainWrapper.standard.integer(forKey: "levelGuess")
            CoinGuess.text = String(Completed!)
            lbLvMainGuess.text = "Score: " + String(Completed!) + " /30"
            progressBarGuess.progress = Float(Completed!)/30.0
        }
        lbLvGuessName.text = String(numberQuestion + 1) + "/30"
    }
    
    @IBAction func backGuess(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "GuessNameViewController") as! GuessNameViewController
        vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.present(vc, animated: true, completion: nil)
    }
}
extension GuessPlayViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == self.AnsCollectionView{
            if SqliteService.shared.setNumberOfSection(number: numberQuestion + 1) == 1
            {
                return 1
            }
            else
            {
                return 2
            }
        }
        else if collectionView == guessCollectionView{
            return 1
        }
        else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.AnsCollectionView{
            if section == 0
            {
                return SqliteService.shared.setNumberOfSection0(number: numberQuestion + 1)
            }
            else
            {
                return SqliteService.shared.setNumberOfSection1(number: numberQuestion + 1)
            }
        }else if collectionView == guessCollectionView{
            return listLetter.count
        }
        else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TextAnsCollectionViewCell", for: indexPath) as! TextAnsCollectionViewCell
        if collectionView == guessCollectionView{
            cell.Letter.textColor = .white
            cell.Letter.text = listLetter[indexPath.item].ans
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        
        return UICollectionReusableView()
    }
    
    func isEmptyLetterArr(letterArr: [String])->Bool{
        if letterArr.count == 0{
            return false
        }else{
            for item in 0...letterArr.count-1{
                if letterArr[item] == ""{
                    return true
                }
            }
        }
        
        return false
    }
    
    func isEmptyListNumber(listNumber: [Int])->Bool{
        if listNumber.count == 0{
            return false
        }else{
            for item in 0...listNumber.count-1{
                if listNumber[item] == -1{
                    return true
                }
            }
        }
        
        return false
    }
    
    func insertLetterArr(letter: String){
        //set LetterArr
        if isEmptyLetterArr(letterArr: letterArr){
            for item in 0...letterArr.count-1{
                if letterArr[item] == ""{
                    letterArr[item] = letter
                    break
                }
            }
        }else{
            self.letterArr.append(letter)
        }
    }
    
    func insertListNumber(number: Int){
        //set ListNumber
        if isEmptyListNumber(listNumber: listNumber){
            for item in 0...listNumber.count-1{
                if listNumber[item] == -1{
                    listNumber[item] = number
                    break
                }
            }
        }else{
            listNumber.append(number)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == guessCollectionView{
            let guessCell = collectionView.cellForItem(at: indexPath) as! TextAnsCollectionViewCell
            if guessCell.Letter.text == ""{
                
            }else{
                if isFullCellInRightAnswerCLVSection0(){
                    
                }else{
                    letter = listLetter[indexPath.item].ans
                    number = listLetter[indexPath.item].id
                    
                    //set LetterArr
                    insertLetterArr(letter: letter)
                    
                    //set ListNumber
                    insertListNumber(number: number)
                    
                    //
                    guessCell.backgroundColor = #colorLiteral(red: 0.9053516984, green: 0.5106465816, blue: 0.5481510758, alpha: 1)
                    guessCell.Letter.text = ""
                    let rightAnswerCell = self.AnsCollectionView.cellForItem(at: getIndexPathOfEmptyTextCellSection0(in: self.AnsCollectionView)!) as! TextAnsCollectionViewCell
                    rightAnswerCell.Letter.textColor = .white
                    rightAnswerCell.Letter.text = letter
                    
                    //If correct answer, present to CorrectViewController
                    if isFullCellInRightAnswerCLVSection0(){
                        if isRightAnswerSection0(){
                            whenAnswerCorrect()
                        }else{
                            whenAnswerFail()
//                            listLettersOfRightAnswer.removeAll()
                        }
                    }
                }
            }
            
        }
        else if collectionView == self.AnsCollectionView{
            let rightAnswerCell = collectionView.cellForItem(at: indexPath) as! TextAnsCollectionViewCell
            if rightAnswerCell.Letter.text!.contains("Label") || isHintLetter(indexPath: indexPath){
                
            }
            else{
                rightAnswerCell.Letter.text = "Label"
                rightAnswerCell.Letter.textColor = #colorLiteral(red: 0.9053516984, green: 0.5106465816, blue: 0.5481510758, alpha: 1)
                if indexPath.section == 0{
                    let guessCell = guessCollectionView.cellForItem(at: IndexPath(item: listNumber[indexPath.item], section: 0)) as! TextAnsCollectionViewCell
                    guessCell.backgroundColor = #colorLiteral(red: 0.9053516984, green: 0.5106465816, blue: 0.5481510758, alpha: 1)
                    guessCell.Letter.text = letterArr[indexPath.item]
                    //set arrays to default
                    letterArr[indexPath.item] = ""
                    listNumber[indexPath.item] = -1
                }
                else {
                    let guessCell = guessCollectionView.cellForItem(at: IndexPath(item: listNumber[indexPath.item + SqliteService.shared.setNumberOfSection0(number: numberQuestion + 1)] , section: 0)) as! TextAnsCollectionViewCell
                    guessCell.backgroundColor = #colorLiteral(red: 0.9053516984, green: 0.5106465816, blue: 0.5481510758, alpha: 1)
                    guessCell.Letter.text = letterArr[indexPath.item + SqliteService.shared.setNumberOfSection0(number: numberQuestion + 1)]
                    //set arrays to default
                    letterArr[indexPath.item + SqliteService.shared.setNumberOfSection0(number: numberQuestion + 1)] = ""
                    listNumber[indexPath.item + SqliteService.shared.setNumberOfSection0(number: numberQuestion + 1)] = -1
                }
            }
        }
    }
    func whenAnswerFail() {

        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoseGuessViewController") as! LoseGuessViewController
        vc.numberQuestion = numberQuestion 
        vc.coin = coin
        vc.score = score
        vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.present(vc, animated: true, completion: nil)
        listNumber.removeAll()
        letterArr.removeAll()
        listLettersOfRightAnswer.removeAll()
    }
    func whenAnswerCorrect(){
        numberQuestion += 1
        switch level {
        case 0:
            KeychainWrapper.standard.removeObject(forKey: "levelGuess")
            KeychainWrapper.standard.set(numberQuestion, forKey: "levelGuess")
        case 1:
            KeychainWrapper.standard.removeObject(forKey: "levelGuess")
            KeychainWrapper.standard.set(numberQuestion, forKey: "levelGuess")
        case 2:
            KeychainWrapper.standard.removeObject(forKey: "levelGuess")
            KeychainWrapper.standard.set(numberQuestion, forKey: "levelGuess")
        case 3:
            KeychainWrapper.standard.removeObject(forKey: "levelGuess")
            KeychainWrapper.standard.set(numberQuestion, forKey: "levelGuess")
        case 4:
            KeychainWrapper.standard.removeObject(forKey: "levelGuess")
            KeychainWrapper.standard.set(numberQuestion, forKey: "levelGuess")
        case 5:
            KeychainWrapper.standard.removeObject(forKey: "levelGuess")
            KeychainWrapper.standard.set(numberQuestion, forKey: "levelGuess")
        default:
            break
        }
        coin += 1
        score += 1
        KeychainWrapper.standard.removeObject(forKey: "coinGuess")
        KeychainWrapper.standard.set(coin, forKey: "coinGuess")
        KeychainWrapper.standard.removeObject(forKey: "scoreGuess")
        KeychainWrapper.standard.set(score, forKey: "scoreGuess")
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "WinGuessViewController") as! WinGuessViewController
        vc.numberQuestion = numberQuestion
        vc.coin = coin
        vc.score = score
        vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.present(vc, animated: true, completion: nil)
        listNumber.removeAll()
        letterArr.removeAll()
        listLettersOfRightAnswer.removeAll()
    }
    func insertLettersForRightAnswerCLVSection0(){
        for item in 0...SqliteService.shared.setNumberOfSection0(number: numberQuestion + 1)-1{
            let cell = AnsCollectionView.cellForItem(at: IndexPath(item: item, section: 0)) as! TextAnsCollectionViewCell
            cell.Letter.text = SqliteService.shared.getRightAnswerLetters(number: numberQuestion + 1)[item]
            cell.Letter.textColor = .white
            if item == SqliteService.shared.setNumberOfSection0(number: numberQuestion + 1)-1{
                if SqliteService.shared.setNumberOfSection(number: numberQuestion + 1) > 1{
                    insertLettersForRightAnswerCLVSection1()
                }
                else{
                    
                }
            }
        }
    }
    
    func insertLettersForRightAnswerCLVSection1(){
        for item in 0...SqliteService.shared.setNumberOfSection1(number: numberQuestion + 1)-1{
            let cell = AnsCollectionView.cellForItem(at: IndexPath(item: item, section: 1)) as! TextAnsCollectionViewCell
            cell.Letter.text = SqliteService.shared.getRightAnswerLetters(number: numberQuestion + 1)[item + SqliteService.shared.setNumberOfSection0(number: numberQuestion + 1)]
            cell.Letter.textColor = .white
        }
    }
    
    func isHintLetter(indexPath: IndexPath)->Bool{
        let cell = AnsCollectionView.cellForItem(at: indexPath)
        if ((cell?.backgroundColor == #colorLiteral(red: 0.9053516984, green: 0.5106465816, blue: 0.5481510758, alpha: 1)) ){
            return true
        } else {
            return false
        }
    }
    
    func isFullCellInRightAnswerCLVSection0()->Bool{
        for item in 0...SqliteService.shared.setNumberOfSection0(number: numberQuestion + 1)-1{
            let cell = AnsCollectionView.cellForItem(at: IndexPath(item: item, section: 0)) as! TextAnsCollectionViewCell
            if ((cell.Letter.text!.contains("Label")) ){
                return false
            }
            if item == SqliteService.shared.setNumberOfSection0(number: numberQuestion + 1)-1{
                if SqliteService.shared.setNumberOfSection(number: numberQuestion + 1) > 1{
                    return isFullCellInRightAnswerCLVSection1()
                }
                else{
                    return true
                }
            }
        }
        return false
    }
    
    func isFullCellInRightAnswerCLVSection1()->Bool{
        for item in 0...SqliteService.shared.setNumberOfSection1(number: numberQuestion + 1)-1{
            let cell = AnsCollectionView.cellForItem(at: IndexPath(item: item, section: 1)) as! TextAnsCollectionViewCell
            if ((cell.Letter.text!.contains("Label")) ){
                return false
            }
            if item == SqliteService.shared.setNumberOfSection1(number: numberQuestion + 1)-1{
                return true
            }
        }
        return false
    }
    
    
    
    func isRightAnswerSection0()->Bool{
        for item in 0...SqliteService.shared.setNumberOfSection0(number: numberQuestion + 1)-1{
            let cell = AnsCollectionView.cellForItem(at: IndexPath(item: item, section: 0)) as! TextAnsCollectionViewCell
            listLettersOfRightAnswer.append(cell.Letter.text!)
            
            if item == SqliteService.shared.setNumberOfSection0(number: numberQuestion + 1)-1{
                if SqliteService.shared.setNumberOfSection(number: numberQuestion + 1) > 1{
                    return isRightAnswerSection1()
                }
                else if SqliteService.shared.setNumberOfSection(number: numberQuestion + 1) == 1{
                    if listLettersOfRightAnswer == SqliteService.shared.getRightAnswerLetters(number: numberQuestion + 1){
                        return true
                    }
                }else{
                    
                }
            }
        }
        return false
    }
    
    func isRightAnswerSection1()->Bool{
        for item in 0...SqliteService.shared.setNumberOfSection1(number: numberQuestion + 1)-1{
            let cell = AnsCollectionView.cellForItem(at: IndexPath(item: item, section: 1)) as! TextAnsCollectionViewCell
            listLettersOfRightAnswer.append(cell.Letter.text!)
            if item == SqliteService.shared.setNumberOfSection1(number: numberQuestion + 1)-1{
                if listLettersOfRightAnswer == SqliteService.shared.getRightAnswerLetters(number: numberQuestion + 1){
                    return true
                }
            }
        }
        return false
    }
    
    func getIndexPathOfEmptyTextCellSection0(in collectionView: UICollectionView) -> IndexPath? {
        
        for item in 0...SqliteService.shared.setNumberOfSection0(number: numberQuestion + 1)-1{
            let cell = collectionView.cellForItem(at: IndexPath(item: item, section: 0)) as! TextAnsCollectionViewCell
            if ((cell.Letter.text!.contains("Label")) ){
                return IndexPath(item: item, section: 0)
            }
            if item == SqliteService.shared.setNumberOfSection0(number: numberQuestion + 1)-1{
                if SqliteService.shared.setNumberOfSection(number: numberQuestion + 1) > 1{
                    let indexPath = getIndexPathOfEmptyTextCellSection1(in: collectionView)
                    return indexPath
                }
            }
        }
        return IndexPath(item: 0, section: 0)
    }
    
    
    
    func getIndexPathOfEmptyTextCellSection1(in collectionView: UICollectionView) -> IndexPath? {
        
        for item in 0...SqliteService.shared.setNumberOfSection1(number: numberQuestion + 1)-1{
            let cell = collectionView.cellForItem(at: IndexPath(item: item, section: 1)) as! TextAnsCollectionViewCell
            if ((cell.Letter.text!.contains("Label"))){
                return IndexPath(item: item, section: 1)
            }
        }
        return IndexPath(item: 0, section: 1)
    }
    
    
    
}
extension GuessPlayViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 10.0, right: 0.0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width: 50, height: 50)
        }
        return CGSize(width: 30, height: 30)
    }
    func centerItemsInCollectionView(cellWidth: Double, numberOfItems: Double, spaceBetweenCell: Double, collectionView: UICollectionView) -> UIEdgeInsets {
        let totalWidth = cellWidth * numberOfItems
        let totalSpacingWidth = spaceBetweenCell * (numberOfItems - 1)
        let leftInset = (collectionView.frame.width - CGFloat(totalWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
    }
    
}

extension UIImage{
    func imageResize (sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        self.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
}

