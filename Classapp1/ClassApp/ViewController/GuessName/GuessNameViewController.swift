//
//  GuessNameViewController.swift
//  ClassApp
//
//  Created by admin on 24/08/2021.
//

import UIKit
import SwiftKeychainWrapper

class GuessNameViewController: UIViewController {
    var listGuess:[GuessModel] = [GuessModel]()
    var score = 0
    var coin = 0
    @IBOutlet weak var CoinGuess: UILabel!
    @IBOutlet weak var lbLvGuessName: UILabel!
    @IBOutlet weak var ProgessBarGuess: UIProgressView!
    @IBOutlet weak var guessCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        super.viewDidLoad()
        ProgessBarGuess.layer.cornerRadius = 5
        ProgessBarGuess.transform = ProgessBarGuess.transform.scaledBy(x: 1, y: 2)
        guessCollectionView.delegate = self
        guessCollectionView.dataSource = self
        guessCollectionView.register(UINib(nibName: GuessChildrenCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: GuessChildrenCollectionViewCell.className)
        
        SqliteService.shared.getDataGuess(){ repond,error in
            if let repond = repond{
                self.listGuess = repond
                self.guessCollectionView.reloadData()
            }
        }
        if (KeychainWrapper.standard.integer(forKey: "levelGuess") == nil) {
            CoinGuess.text = "0"
            lbLvGuessName.text = "Score: 0/30"
            ProgessBarGuess.progress = 0
        }
        else{
            let Completed = KeychainWrapper.standard.integer(forKey: "levelGuess")
            CoinGuess.text = String(Completed!)
            lbLvGuessName.text = "Score: " + String(Completed!) + " /30"
            ProgessBarGuess.progress = Float(Completed!)/30.0
        }
        if KeychainWrapper.standard.integer(forKey: "scoreGuess") == nil {
            KeychainWrapper.standard.set(0, forKey: "scoreGuess")
        }
        score = KeychainWrapper.standard.integer(forKey: "scoreGuess")!
        if KeychainWrapper.standard.integer(forKey: "coinGuess") == nil {
            KeychainWrapper.standard.set(0, forKey: "coinGuess")
        }
        coin = KeychainWrapper.standard.integer(forKey: "coinGuess")!
    }
    
    @IBAction func backMain(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.present(vc, animated: true)
    }
}
extension GuessNameViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "GuessPlayViewController") as! GuessPlayViewController
        vc.level = indexPath.item
        switch vc.level {
        case 0:
            if (KeychainWrapper.standard.integer(forKey: "levelGuess") == nil){
                if (KeychainWrapper.standard.integer(forKey: "scoreGuess") == nil && KeychainWrapper.standard.integer(forKey: "coinGuess") == nil){
                    KeychainWrapper.standard.set(0, forKey: "scoreGuess")
                    KeychainWrapper.standard.set(0, forKey: "coinGuess")
                }
                KeychainWrapper.standard.set(0, forKey: "levelGuess")
                vc.score = KeychainWrapper.standard.integer(forKey: "scoreGuess")!
                vc.coin = KeychainWrapper.standard.integer(forKey: "coinGuess")!
            }else{
                let numberQuestion = KeychainWrapper.standard.integer(forKey: "levelGuess")!
                if (KeychainWrapper.standard.integer(forKey: "scoreGuess") == nil && KeychainWrapper.standard.integer(forKey: "coinGuess") == nil){
                    KeychainWrapper.standard.set(0, forKey: "scoreGuess")
                    KeychainWrapper.standard.set(0,forKey: "coinGuess")
                }
                vc.score = KeychainWrapper.standard.integer(forKey: "scoreGuess")!
                vc.coin = KeychainWrapper.standard.integer(forKey: "coinGuess")!
                if numberQuestion >= 5{
                    vc.numberQuestion = 0
                    vc.score -= 5
                }else{
                    vc.numberQuestion = numberQuestion
                    
                }
                vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                self.present(vc, animated: true)
            }
            
        case 1:
            if score >= 5{
                if (KeychainWrapper.standard.integer(forKey: "levelGuess") == nil){
                    if (KeychainWrapper.standard.integer(forKey: "scoreGuess") == nil && KeychainWrapper.standard.integer(forKey: "coinGuess") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreGuess")
                        KeychainWrapper.standard.set(0, forKey: "coinGuess")
                    }
                    KeychainWrapper.standard.set(0, forKey: "levelGuess")
                    //                vc.numberQuestion = KeychainWrapper.standard.integer(forKey: "levelGuess")!
                    vc.score = KeychainWrapper.standard.integer(forKey: "scoreGuess")!
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinGuess")!
                }else{
                    let numberQuestion = KeychainWrapper.standard.integer(forKey: "levelGuess")!
                    if (KeychainWrapper.standard.integer(forKey: "scoreGuess") == nil && KeychainWrapper.standard.integer(forKey: "coinGuess") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreGuess")
                        KeychainWrapper.standard.set(0,forKey: "coinGuess")
                    }
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinGuess")!
                    vc.score = KeychainWrapper.standard.integer(forKey: "scoreGuess")!
                    if numberQuestion >= 10{
                        vc.numberQuestion = 5
                        vc.score -= 5
                    }else{
                        vc.numberQuestion = numberQuestion
                    }
                }
                vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                self.present(vc, animated: true)
            }
            
        case 2:
            if score >= 10{
                if (KeychainWrapper.standard.integer(forKey: "levelGuess") == nil){
                    if (KeychainWrapper.standard.integer(forKey: "scoreGuess") == nil && KeychainWrapper.standard.integer(forKey: "coinGuess") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreGuess")
                        KeychainWrapper.standard.set(0, forKey: "coinGuess")
                    }
                    KeychainWrapper.standard.set(0, forKey: "levelGuess")
                    //                vc.numberQuestion = KeychainWrapper.standard.integer(forKey: "levelGuess")!
                    vc.score = KeychainWrapper.standard.integer(forKey: "scoreGuess")!
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinGuess")!
                }else{
                    let numberQuestion = KeychainWrapper.standard.integer(forKey: "levelGuess")!
                    if (KeychainWrapper.standard.integer(forKey: "scoreGuess") == nil && KeychainWrapper.standard.integer(forKey: "coinGuess") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreGuess")
                        KeychainWrapper.standard.set(0,forKey: "coinGuess")
                    }
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinGuess")!
                    vc.score = KeychainWrapper.standard.integer(forKey: "scoreGuess")!
                    if numberQuestion >= 15{
                        vc.numberQuestion = 10
                        vc.score -= 5
                    }else{
                        vc.numberQuestion = numberQuestion
                    }
                }
                vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                self.present(vc, animated: true)
            }
        case 3:
            if score >= 15 {
                if (KeychainWrapper.standard.integer(forKey: "levelGuess") == nil){
                    if (KeychainWrapper.standard.integer(forKey: "scoreGuess") == nil && KeychainWrapper.standard.integer(forKey: "coinGuess") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreGuess")
                        KeychainWrapper.standard.set(0, forKey: "coinGuess")
                    }
                    KeychainWrapper.standard.set(0, forKey: "levelGuess")
                    //                vc.numberQuestion = KeychainWrapper.standard.integer(forKey: "levelGuess")!
                    vc.score = KeychainWrapper.standard.integer(forKey: "scoreGuess")!
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinGuess")!
                }else{
                    let numberQuestion = KeychainWrapper.standard.integer(forKey: "levelGuess")!
                    if (KeychainWrapper.standard.integer(forKey: "scoreGuess") == nil && KeychainWrapper.standard.integer(forKey: "coinGuess") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreGuess")
                        KeychainWrapper.standard.set(0,forKey: "coinGuess")
                    }
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinGuess")!
                    vc.score = KeychainWrapper.standard.integer(forKey: "scoreGuess")!
                    if numberQuestion >= 20{
                        vc.numberQuestion = 15
                        vc.score -= 5
                    }else{
                        vc.numberQuestion = numberQuestion
                    }
                }
                vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                self.present(vc, animated: true)
            }
        case 4:
            if score >= 20{
                if (KeychainWrapper.standard.integer(forKey: "levelGuess") == nil){
                    if (KeychainWrapper.standard.integer(forKey: "scoreGuess") == nil && KeychainWrapper.standard.integer(forKey: "coinGuess") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreGuess")
                        KeychainWrapper.standard.set(0, forKey: "coinGuess")
                    }
                    KeychainWrapper.standard.set(0, forKey: "levelGuess")
                    //                vc.numberQuestion = KeychainWrapper.standard.integer(forKey: "levelGuess")!
                    vc.score = KeychainWrapper.standard.integer(forKey: "scoreGuess")!
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinGuess")!
                }else{
                    let numberQuestion = KeychainWrapper.standard.integer(forKey: "levelGuess")!
                    if (KeychainWrapper.standard.integer(forKey: "scoreGuess") == nil && KeychainWrapper.standard.integer(forKey: "coinGuess") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreGuess")
                        KeychainWrapper.standard.set(0,forKey: "coinGuess")
                    }
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinGuess")!
                    vc.score = KeychainWrapper.standard.integer(forKey: "scoreGuess")!
                    if numberQuestion >= 20{
                        vc.numberQuestion = 20
                        vc.score -= 5
                    }else{
                        vc.numberQuestion = numberQuestion
                    }
                }
                vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                self.present(vc, animated: true)
            }
        case 5:
            if score >= 25 {
                if (KeychainWrapper.standard.integer(forKey: "levelGuess") == nil){
                    if (KeychainWrapper.standard.integer(forKey: "scoreGuess") == nil && KeychainWrapper.standard.integer(forKey: "coinGuess") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreGuess")
                        KeychainWrapper.standard.set(0, forKey: "coinGuess")
                    }
                    KeychainWrapper.standard.set(0, forKey: "levelGuess")
                    //                vc.numberQuestion = KeychainWrapper.standard.integer(forKey: "levelGuess")!
                    vc.score = KeychainWrapper.standard.integer(forKey: "scoreGuess")!
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinGuess")!
                }else{
                    let numberQuestion = KeychainWrapper.standard.integer(forKey: "levelGuess")!
                    if (KeychainWrapper.standard.integer(forKey: "scoreGuess") == nil && KeychainWrapper.standard.integer(forKey: "coinGuess") == nil){
                        KeychainWrapper.standard.set(0, forKey: "scoreGuess")
                        KeychainWrapper.standard.set(0,forKey: "coinGuess")
                    }
                    vc.coin = KeychainWrapper.standard.integer(forKey: "coinGuess")!
                    vc.score = KeychainWrapper.standard.integer(forKey: "scoreGuess")!
                    if numberQuestion >= 25{
                        vc.numberQuestion = 25
                        vc.score -= 5
                    }else{
                        vc.numberQuestion = numberQuestion
                    }
                }
                vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                self.present(vc, animated: true)
            }
            
        default:
            break
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GuessChildrenCollectionViewCell", for: indexPath) as! GuessChildrenCollectionViewCell
        cell.imgContinue.isHidden = true
        cell.imgGuess1.isHidden = true
        cell.imgGuess2.isHidden = true
        cell.imgGuess3.isHidden = true
        cell.imgPlay.isHidden = true
        cell.imgLocked.isHidden = true
        cell.lbLocked.isHidden = true
        cell.lbPlayGuess.isHidden = true
        
        switch indexPath.item {
        case 0:
            cell.imgGuess1.isHidden = false
            cell.imgGuess2.isHidden = false
            cell.imgGuess3.isHidden = false
            cell.imgPlay.isHidden = false
            cell.lbPlayGuess.isHidden = false
            cell.lbLevelGuess.text = "Level 1"
        case 1:
            cell.lbLevelGuess.text = "Level 2"
            if score >= 5 {
                cell.imgGuess1.isHidden = false
                cell.imgGuess2.isHidden = false
                cell.imgGuess3.isHidden = false
                cell.imgPlay.isHidden = false
                cell.lbPlayGuess.isHidden = false
            }
            else
            {
                cell.imgContinue.isHidden = false
                cell.imgLocked.isHidden = false
                cell.lbLocked.isHidden = false
            }
        case 2:
            cell.lbLevelGuess.text = "Level 3"
            if score >= 10 {
                cell.imgGuess1.isHidden = false
                cell.imgGuess2.isHidden = false
                cell.imgGuess3.isHidden = false
                cell.imgPlay.isHidden = false
                cell.lbPlayGuess.isHidden = false
            }
            else
            {
                cell.imgContinue.isHidden = false
                cell.imgLocked.isHidden = false
                cell.lbLocked.isHidden = false
            }
        case 3:
            cell.lbLevelGuess.text = "Level 4"
            if score >= 15 {
                cell.imgGuess1.isHidden = false
                cell.imgGuess2.isHidden = false
                cell.imgGuess3.isHidden = false
                cell.imgPlay.isHidden = false
                cell.lbPlayGuess.isHidden = false
            }
            else
            {
                cell.imgContinue.isHidden = false
                cell.imgLocked.isHidden = false
                cell.lbLocked.isHidden = false
            }
        case 4:
            cell.lbLevelGuess.text = "Level 5"
            if score >= 20 {
                cell.imgGuess1.isHidden = false
                cell.imgGuess2.isHidden = false
                cell.imgGuess3.isHidden = false
                cell.imgPlay.isHidden = false
                cell.lbPlayGuess.isHidden = false
            }
            else
            {
                cell.imgContinue.isHidden = false
                cell.imgLocked.isHidden = false
                cell.lbLocked.isHidden = false
            }
        case 5:
            cell.lbLevelGuess.text = "Level 6"
            if score >= 25 {
                cell.imgGuess1.isHidden = false
                cell.imgGuess2.isHidden = false
                cell.imgGuess3.isHidden = false
                cell.imgPlay.isHidden = false
                cell.lbPlayGuess.isHidden = false
            }
            else
            {
                cell.imgContinue.isHidden = false
                cell.imgLocked.isHidden = false
                cell.lbLocked.isHidden = false
            }
        default:
            break
        }
        return cell
    }
}
extension GuessNameViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width: UIScreen.main.bounds.width/2-10, height:  300)
        }
        return CGSize(width: UIScreen.main.bounds.width/2-10, height:  200)
    }
    
}

