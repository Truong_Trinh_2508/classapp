//
//  DailyRewardViewController.swift
//  ClassApp
//
//  Created by admin on 23/08/2021.
//

import UIKit

class DailyRewardViewController: UIViewController{
    
    @IBOutlet weak var btnClaim: UIButton!
    var listDayDaily:[String] = ["Day 1","Day 2","Day 3","Day 4","Day 5","Day 6","Day 7"]
    @IBOutlet weak var dailyCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        dailyCollectionView.delegate = self
        dailyCollectionView.dataSource = self
        dailyCollectionView.register(UINib(nibName: DailyRewardCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: DailyRewardCollectionViewCell.className)
    }
    
    @IBAction func backMain(_ sender: Any) {
        self.dismiss(animated: true)
    }
}
extension DailyRewardViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listDayDaily.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DailyRewardCollectionViewCell.className, for: indexPath) as! DailyRewardCollectionViewCell
        cell.lbDay.text = listDayDaily[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        
        return UICollectionReusableView()
    }
    
}
extension DailyRewardViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width: UIScreen.main.bounds.width , height: 120)
        }
        return CGSize(width: UIScreen.main.bounds.width, height: 100)
    }
}
